import React from "react";
import styled from "styled-components";
import "@reach/tooltip/styles.css";

import Header from "./Header";
import PageContent from "./PageContent";
import GlobalStyles from "./GlobalStyles";
import HelpButton from "./HelpButton";

export default function App() {
  return (
    <>
      <Wrapper>
        <HeaderWrapper>
          <Header />
        </HeaderWrapper>
        <PageContent />
      </Wrapper>
      <HelpButton />
      <GlobalStyles />
    </>
  );
}

const Wrapper = styled.div`
  isolation: isolate;
  min-height: 150vh;
`;

const HeaderWrapper = styled.div`
  z-index: 1;
  position: sticky;
  top: 0;
`;
